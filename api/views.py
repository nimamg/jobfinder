from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated, BasePermission

from users.models import Applicant, Employer, User
from jobs.models import Job, Application
from .serializers import ApplicantSerializer, EmployerSerializer, JobSerializer, ApplicationSerializer

class ApplicantViewset(viewsets.ModelViewSet):
    queryset = Applicant.objects.all()
    serializer_class = ApplicantSerializer

    def create(self, request):
        serializer = ApplicantSerializer(data=request.data) 
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)

    permission_classes_by_action = {'create': [AllowAny]}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            return [permission() for permission in self.permission_classes]
    


class EmployerViewset(viewsets.ModelViewSet):
    queryset = Employer.objects.all()
    serializer_class = EmployerSerializer


    def create(self, request):
        serializer = EmployerSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)

    permission_classes_by_action = {'create': [AllowAny]}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            return [permission() for permission in self.permission_classes]
    
class isEmployer (BasePermission):

    def has_permission(self, request, view):
        print("HERE ", User.objects.get(username=request.user).is_employer)
        if User.objects.get(username=request.user).is_employer == True:
            return True
        return False


class isOwner(BasePermission):

    def has_permission(self, request, view):
        try:
            jobId=view.kwargs['pk']
            job = Job.objects.get(id=jobId)
            ownerId = job.owner.user.id
        except:
            return False
        if request.user.id == ownerId:
            return True
        return False


class JobViewset(viewsets.ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

    permission_classes_by_action = {'list': [AllowAny],
                                    'retrieve': [AllowAny],
                                    'create': [isEmployer],
                                    'partial_update': [isOwner],
                                    'update': [isOwner],
                                    'delete': [isOwner]
    }

    def create(self, request):
        data = request.data
        data['owner'] = request.user
        serializer = JobSerializer(data=data)
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)
    
    # def perform_create(self, serializer):
    #     print("DO WE?")
    #     serializer.save(owner=User.objects.get(username=self.request.user))



    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError: 
            return [permission() for permission in self.permission_classes]

class ApplyView(CreateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

    def create(self, request):
        serializer = ApplicationSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            print("AAAAAAAAAAAAAAAAAa")
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)

class AcceptRejectView(UpdateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer

    def partial_update(self, validated_data):
        jobId = validated_data.pop('job')
        if (validated_data.pop('status') == 1):
            serializer = ApplicationSerializer(data=jobId)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.accept(validated_data=jobId)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)
        elif (validated_data.pop('status') == 2):
            serializer = ApplicationSerializer(data=jobId)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.accept(validated_data=jobId)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.error_messages,
                        status=status.HTTP_400_BAD_REQUEST)
