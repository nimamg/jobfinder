from rest_framework import serializers
from users.models import Employer, Applicant, Skill, User, jobField
from jobs.models import Job, Application

class UserSerializer (serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'phoneNo']

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user
        
class SkillSerializer (serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = ['title']

class ApplicantSerializer (serializers.ModelSerializer):
    skills = SkillSerializer(many=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = Applicant
        fields = ['user', 'age', 'gender', 'skills', 'cv']
    
    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_data['is_applicant'] = True
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        applicant, created = Applicant.objects.update_or_create(user=user, age=validated_data.pop('age'), gender=validated_data.pop('gender'), cv=validated_data.pop('cv'))
        skills = validated_data.pop('skills')
        for i in skills:
            newSkill = SkillSerializer.create(SkillSerializer(), validated_data=i)
            applicant.skills.add(newSkill)
        return applicant

class JobFieldSerializer (serializers.ModelSerializer):
    class Meta:
        model = jobField
        fields = ['title']

class EmployerSerializer (serializers.ModelSerializer):
    jobField = JobFieldSerializer()
    user = UserSerializer(read_only=True)

    class Meta:
        model = Employer
        fields = ['user', 'companyName', 'foundingDate', 'address', 'jobField']

    def create(self, validated_data):
        print(validated_data)
        user_data = validated_data.pop('user')
        user_data['is_employer'] = True
        user = UserSerializer.create(UserSerializer(), validated_data=user_data)
        jobField_data = validated_data.pop('jobField')
        jobField = JobFieldSerializer.create(JobFieldSerializer(), validated_data=jobField_data)
        employer, created = Employer.objects.update_or_create(user=user, companyName=validated_data.pop('companyName'), 
            foundingDate=validated_data.pop('foundingDate'), address=validated_data.pop('address'), jobField=jobField)

class JobSerializer (serializers.ModelSerializer):
    neededSkills = SkillSerializer(many=True)
    owner = EmployerSerializer(read_only=True)

    class Meta:
        model = Job
        fields = ['id', 'title', 'jobDesc', 'expiryDate', 'salary', 'owner', 'neededSkills', 'img', 'weekday_from', 'weekday_to', 'from_hour', 'to_hour']
    
    def create(self, validated_data):
        owner = User.objects.get(username=validated_data.pop('owner'))
        print("*********** ", owner.employer)
        job, created = Job.objects.update_or_create(title=validated_data.pop('title'), 
            jobDesc=validated_data.pop('jobDesc'), expiryDate=validated_data.pop('expiryDate'), 
            salary=validated_data.pop('salary'), img=validated_data.pop('img'),
            weekday_from=validated_data.pop('weekday_from'), weekday_to=validated_data.pop('weekday_to'),
            from_hour=validated_data.pop('from_hour'), to_hour=validated_data.pop('to_hour'), owner=owner.employer)

class ApplicationSerializer (serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['applicant', 'job']

    def create(self, validated_data):
        print(" NOW WHAT?")
        applicantId = validated_data.pop('applicant')
        user = User.objects.get(id = applicantId)
        applicant = user.applicant
        application, created = Application.objects.update_or_create(applicant=applicant, job=Job.objects.get(id=validated_data.pop('job')), status=0)
        
    def accept(self, validated_data):
        application = Application.get(validated_data.pop('application'))
        application.status = 1

    def reject(self, validated_data):
        application = Application.get(validated_data.pop('application'))
        application.status = 2