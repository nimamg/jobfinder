from django.urls import path, include

from .views import ApplicantViewset, EmployerViewset, JobViewset, ApplyView, AcceptRejectView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('applicants',ApplicantViewset, base_name='applicants')
router.register('employers', EmployerViewset, base_name='employers')
router.register('jobs', JobViewset, base_name='jobs')

urlpatterns = [
    path('rest-auth/', include('rest_auth.urls')),
    path('apply/', ApplyView.as_view(), name='apply'),
    path('acceptreject/', AcceptRejectView.as_view()),
] 

urlpatterns += router.urls

