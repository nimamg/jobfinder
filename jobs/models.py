from django.db import models
from users.models import Skill, Employer, Applicant

class Job(models.Model):
    title = models.CharField(max_length=40)
    jobDesc = models.TextField()
    neededSkills = models.ManyToManyField(Skill)
    owner = models.ForeignKey(Employer, on_delete=models.CASCADE, related_name='jobs', null=True)
    expiryDate = models.DateField()
    salary = models.PositiveIntegerField()
    img = models.ImageField(upload_to='images/', null=True)

    HOUR_OF_DAY_24 = [(i,i) for i in range(1,25)]

    WEEKDAYS = [
    (1, "Monday"),
    (2, "Tuesday"),
    (3, "Wednesday"),
    (4, "Thursday"),
    (5, "Friday"),
    (6, "Saturday"),
    (7, "Sunday"),
    ]

    weekday_from = models.PositiveSmallIntegerField(choices=WEEKDAYS)
    weekday_to = models.PositiveSmallIntegerField(choices=WEEKDAYS)
    from_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)
    to_hour = models.PositiveSmallIntegerField(choices=HOUR_OF_DAY_24)

    def __str__(self):
        return self.title

class Application(models.Model):
    applicant = models.ForeignKey(Applicant, on_delete=models.CASCADE, related_name='applications')
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name='applications')
    ### STATUS CODES: Unattended = 0, Accepted = 1, Rejected = 2
    status = models.PositiveIntegerField()