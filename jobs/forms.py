from django.forms import forms, ModelForm
from .models import Job

class JobCreationForm(ModelForm):
    class Meta:
        model = Job
        fields = ['title', 'jobDesc', 'neededSkills', 'expiryDate', 'salary', 'img', 'weekday_from', 'weekday_to', 'from_hour', 'to_hour']
