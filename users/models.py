from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from django.dispatch import receiver
from django.db.models.signals import post_delete


class User(AbstractUser):
    is_applicant = models.BooleanField(default=False)
    is_employer = models.BooleanField(default=False)
    phoneNo = PhoneNumberField()


class Skill(models.Model):
    title=models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Applicant(models.Model):

    GENDER_CHOICES = (('M', 'Male'),
                    ('F', 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,related_name='applicant')
    age = models.PositiveIntegerField(null=True, blank=True)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES) 
    cv = models.FileField(upload_to='CVs/', null=True)
    skills = models.ManyToManyField(Skill, related_name='skilled_applicants')

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)

    def delete(self, *args, **kwargs):
        self.user.delete()
        return super(self.__class__, self).delete(*args, **kwargs)




class jobField(models.Model):
    title = models.CharField(max_length=30)

    def __str__(self):
        return self.title

class Employer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True,related_name='employer')
    companyName = models.CharField(max_length=40)
    foundingDate = models.DateField()
    address = models.TextField()
    jobField = models.ForeignKey(jobField, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.companyName

    def delete(self, *args, **kwargs):
        self.user.delete()
        return super(self.__class__, self).delete(*args, **kwargs)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        token = Token.objects.create(user=instance)
        print("Token Key ", token.key)