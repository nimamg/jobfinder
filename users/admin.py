from django.contrib import admin

from .models import User, Applicant, Employer

admin.site.register(Applicant)
admin.site.register(User)
admin.site.register(Employer)
