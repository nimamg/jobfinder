from django.shortcuts import render
from rest_framework import generics

from django.views.generic import CreateView

from .forms import ApplicantUserCreationForm, EmployerUserCreationForm
from .models import User, Applicant, Employer, Skill
    
class ApplicantSignupView(CreateView):
    model = User
    form_class = ApplicantUserCreationForm
    template_name = 'signup.html'

class EmployerSignupView(CreateView):
    model = User
    form_class = EmployerUserCreationForm
    template_name = 'signup.html'
