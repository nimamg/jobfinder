from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from phonenumber_field.formfields import PhoneNumberField
from django.db import transaction

from .models import Applicant, Employer, Skill, User

class ApplicantUserCreationForm(UserCreationForm):

    GENDER_CHOICES = (('M', 'Male'),
                      ('F', 'Female'),
    )

    Age = forms.IntegerField(min_value=0,)
    gender = forms.ChoiceField(choices=GENDER_CHOICES,required=True,label='Gender')
    phone = PhoneNumberField(min_length=12, max_length=13,label= 'Phone Number')

    skills = forms.ModelMultipleChoiceField(
        queryset=Skill.objects.all(),
        widget=forms.CheckboxSelectMultiple,    
        required=False
    )

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('email', 'first_name', 'last_name',)
    
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.phoneNo = self.cleaned_data.get('phone')
        user.is_applicant = True
        user.save()
        applicant = Applicant.objects.create(user=user, age = self.cleaned_data.get('Age'), gender = self.cleaned_data.get('gender'))
        applicant.skills.add(*self.cleaned_data.get('skills'))
        return applicant

class ApplicantChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email',)


class EmployerUserCreationForm(UserCreationForm):
    companyName = forms.CharField(max_length=30, label='Company Name')
    foundationDate = forms.DateField(required=True, label= 'Foundation Date', widget=forms.DateInput(attrs={'type': 'date'})) #forms.SelectDateWidget
    address = forms.CharField(widget=forms.Textarea, label='Address')
    phone = PhoneNumberField(min_length=12, max_length=13,label= 'Phone Number')

        
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('email', 'first_name', 'last_name',)

    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.phoneNo = self.cleaned_data.get('phone')
        user.is_employer = True
        user.save()
        employer = Employer.objects.create(user=user, foundingDate=self.data['foundationDate'], 
                                           companyName = self.cleaned_data.get('companyName'), address = self.cleaned_data.get('address'))
        # employer.companyName = self.cleaned_data.get('companyName')
        # employer.foundingDate = self.data['foundationDate']
        # employer.address = self.cleaned_data.get('address')
        return employer

class EmployerChangeForm(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'email',)
