from django.urls import path

from .views import ApplicantSignupView, EmployerSignupView

urlpatterns = [
    path('applicantsignup/', ApplicantSignupView.as_view(), name='applicantSignup'),
    path('employersignup/', EmployerSignupView.as_view(), name='employerSignup'),
]